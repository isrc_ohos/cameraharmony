package com.huawei.mytestapp;


import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.media.codec.Codec;
import ohos.media.common.BufferInfo;
import ohos.media.common.Format;

import java.nio.ByteBuffer;

public class VDDecoder implements SurfaceOps.Callback {
    //解码分辨率
    private SurfaceOps holder;
    private Surface mSurface;
    //解码器
    private Codec mCodec;
    //是否播放
    private boolean isDecoder = false;// 判断是否开始解码
    private boolean isMediaCodecInit = false;// 判断解码器是否初始化
    private boolean isSurfaceCreated = false;// 判断Surface是否被创建

    /**
     * 设置Surface和SurfaceOps
     */
    public VDDecoder(SurfaceProvider playerView) {
        // 设置 SurfaceProvider，即使用 surfaceview2 播放解码后的视频
        this.holder = playerView.getSurfaceOps().get();
        playerView.pinToZTop(true);
        holder.addCallback(this);// 设置回调
        mCodec = Codec.createDecoder();// 创建解码器

        // 设置该组件让屏幕不会自动关闭
        holder.setKeepScreenOn(true);
    }

    @Override
    public void surfaceChanged(SurfaceOps holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceOps holder) {
        isSurfaceCreated = true;
        mSurface = holder.getSurface();
        beginCodec();// 准备初始化解码器
        mLog.log("surfacecreated", "surfaceCreated!!!!!!");
    }

    @Override
    public void surfaceDestroyed(SurfaceOps holder) {
        isSurfaceCreated = false;
        if (isMediaCodecInit) {
            mCodec.stop();
            isMediaCodecInit = false;
        }
    }

    /**
     * NV21 数据顺时针旋转指定角度
     */
    public void rotateNV21(byte[] input, byte[] output, int width, int height, int rotation) {
        boolean swap = (rotation == 90 || rotation == 270);
        boolean yflip = (rotation == 90 || rotation == 180);
        boolean xflip = (rotation == 270 || rotation == 180);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int xo = x, yo = y;
                int w = width, h = height;
                int xi = xo, yi = yo;
                if (swap) {
                    xi = w * yo / h;
                    yi = h * xo / w;
                }
                if (yflip) {
                    yi = h - yi - 1;
                }
                if (xflip) {
                    xi = w - xi - 1;
                }
                output[w * yo + xo] = input[w * yi + xi];
                int fs = w * h;
                xi = (xi >> 1);
                yi = (yi >> 1);
                xo = (xo >> 1);
                yo = (yo >> 1);
                w = (w >> 1);
                h = (h >> 1);
                // adjust for interleave here
                int ui = fs + (w * yi + xi) * 2;
                int uo = fs + (w * yo + xo) * 2;
                // and here
                int vi = ui + 1;
                int vo = uo + 1;
                output[uo] = input[ui];
                output[vo] = input[vi];
            }
        }
    }


    private Codec.ICodecListener decoderlistener = new Codec.ICodecListener() {
        // 用于监听编码器，获取解码完成后的数据
        @Override
        public void onReadBuffer(ByteBuffer byteBuffer, BufferInfo bufferInfo, int i) {
            byte[] bytes = new byte[bufferInfo.size];
            byte[] rotate_bytes = new byte[bufferInfo.size];

            byteBuffer.get(bytes);// 从解码器的 byteBuffer 中获取数据
            mLog.log("bytes", "byte:" + bytes);

            // 将解码后的 NV21(YUV420SP) 数据 bytes 顺时针旋转 90 度，并通过 Surface 显示
            rotateNV21(bytes, rotate_bytes, 640, 480, 90);// 旋转后的数据用 rotate_bytes 存放
            // 渲染旋转后的数据 rotate_bytes 通过mSurface显示出来，第二个参数是待渲染的数据格式即YUV420SP
            mSurface.showRawImage(rotate_bytes, Surface.PixelFormat.PIXEL_FORMAT_YCRCB_420_SP, 640, 480);

        }

        @Override
        public void onError(int i, int i1, int i2) {

        }
    };

    /**
     * 初始化解码器各参数
     */
    private synchronized void beginCodec() {
        System.out.println("isSurfaceCreated = " + Boolean.toString(isSurfaceCreated));
        if (isSurfaceCreated) {
            isSurfaceCreated = false;
            Format fmt = new Format();// 创建解码器格式
            fmt.putStringValue("mime", "video/avc");
            fmt.putIntValue("width", 640);// 视频图像宽度
            fmt.putIntValue("height", 480);// 视频图像高度
            fmt.putIntValue("bitrate", 392000);// 比特率
            fmt.putIntValue("color-format", 21);// 颜色格式
            fmt.putIntValue("frame-rate", 30);// 帧率
            fmt.putIntValue("i-frame-interval", -1);// 关键帧间隔时间
            fmt.putIntValue("bitrate-mode", 1);// 比特率模式

            mCodec.setCodecFormat(fmt);// 设置解码器格式
            mCodec.registerCodecListener(decoderlistener);// 设置监听
            mCodec.start();// 解码器开始执行
            isMediaCodecInit = true;
        }
    }

    public void start() {
        isDecoder = true;
    }

    /**
     * 停止解码
     */
    public void stop() {
        isDecoder = false;
    }

    public void destroy() {
        isSurfaceCreated = false;
        isDecoder = false;
        if (isMediaCodecInit) {
            isMediaCodecInit = false;
            mCodec.release();
            mCodec = null;
        }
    }

    public void toDecoder(byte[] video){
        if (isMediaCodecInit) {
            decoder(video);// 解码
            mLog.log("videocallback", "length:" + video.length);
        }
    }

    /**
     * 开始解码，将数据以 Buffer 和 BufferUnfo 的形式通过 Codec 类进行解码
     */
    private void decoder(byte[] video) {
        ByteBuffer mBuffer = mCodec.getAvailableBuffer(-1);
        BufferInfo info = new BufferInfo();
        info.setInfo(0, video.length, 0, 0);
        mBuffer.put(video);
        mCodec.writeBuffer(mBuffer, info);
    }
}