package com.huawei.mytestapp;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 自定义log，方便使用
 */

public class mLog {
    public static void log(String TAG, String content) {
        HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, TAG);
        HiLog.info(LABEL_LOG,content);
    }
}
