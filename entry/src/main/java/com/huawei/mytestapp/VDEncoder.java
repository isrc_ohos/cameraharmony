package com.huawei.mytestapp;

import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.media.codec.Codec;
import ohos.media.common.BufferInfo;
import ohos.media.common.Format;

import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;

public class VDEncoder {
    Codec mCodec = Codec.createEncoder();// 创建Harmony编码器
    public boolean isRuning = false;// 判断编码行为是否执行
    public final int QueueNum = 300;
    // 创建视频队列，将从相机获取的原始视频数据挨个放入队列
    private ArrayBlockingQueue<byte[]> YUVQueue = new ArrayBlockingQueue<>(QueueNum);
    private com.huawei.mytestapp.SingleThreadExecutor singleThreadExecutor;// 使用自定义单例线程池

    VDDecoder vdDecoder;// 创建解码类对象

    private Codec.ICodecListener encoderlistener = new Codec.ICodecListener() {
        // 用于监听编码器，获取编码完成后的数据
        @Override
        public void onReadBuffer(ByteBuffer byteBuffer, BufferInfo bufferInfo, int i) {
            byte[] data = new byte[bufferInfo.size];
            byteBuffer.get(data);// 从编码器的 byteBuffer 中获取数据
            mLog.log("pushdata", "encoded data:" + data.length);
            vdDecoder.toDecoder(data);// 通过解码类的 toDecoder 方法，将编码完成的视频数据送去解码
        }

        @Override
        public void onError(int i, int i1, int i2) {
            throw new RuntimeException();
        }
    };

    /**
     * 初始化编码器
     */
    public VDEncoder(int framerate){
        Format fmt = new Format();// 创建编码器格式
        fmt.putStringValue("mime", "video/avc");
        fmt.putIntValue("width", 640);// 视频图像宽度
        fmt.putIntValue("height", 480);// 视频图像高度
        fmt.putIntValue("bitrate", 392000);// 比特率
        fmt.putIntValue("color-format", 21);// 颜色格式
        fmt.putIntValue("frame-rate", framerate);// 帧率
        fmt.putIntValue("i-frame-interval", 1);// 关键帧间隔时间
        fmt.putIntValue("bitrate-mode", 1);// 比特率模式

        mCodec.setCodecFormat(fmt);// 设置编码器格式
        mCodec.registerCodecListener(encoderlistener);// 设置监听
        mCodec.start();// 编码器开始执行
        singleThreadExecutor = new SingleThreadExecutor();// 初始化自定义单例线程池
    }

    public void destroy() {
        isRuning = false;
        mCodec.release();
        mCodec = null;
        singleThreadExecutor.shutdownNow();
    }

    /**
     * 用于将从相机获取到的数据加入到队列
     */
    public void addFrame(byte[] bytes) {
        if (isRuning) {
            addQueue(YUVQueue, bytes);
        }
    }

    /**
     * 开始编码
     */
    public void start() {
        YUVQueue.clear();
        isRuning = true;
        startEncoderThread();// 使用自定义单例线程池，开始编码线程
    }

    /**
     * 编码线程
     */
    private void startEncoderThread() {
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                byte[] data;
                while (isRuning) {
                    try {
                        data = YUVQueue.take();// 从队列中获取原相机得到的原生视频数据
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                    // 将数据以 Buffer 和 BufferUnfo 的形式通过 Codec 类进行编码
                    ByteBuffer buffer =  mCodec.getAvailableBuffer(-1);
                    BufferInfo bufferInfo = new BufferInfo();
                    buffer.put(data);
                    bufferInfo.setInfo(0, data.length, System.currentTimeMillis(), 0);
                    mCodec.writeBuffer(buffer, bufferInfo);
                }
            }
        });
    }

    /**
     * 将元素依次加入队列
     */
    public <T> void addQueue(ArrayBlockingQueue<T> queue, T t) {
        if (queue.size() >= QueueNum) {
            queue.poll();
        }
        queue.offer(t);
    }

    public void prepareDecoder(SurfaceProvider surfaceview){
        vdDecoder = new VDDecoder(surfaceview);// 创建解码类对象，并使用surfaceview显示解码后的视频
        vdDecoder.start();// 开始解码
    }

}
