package com.huawei.mytestapp.slice;

import com.huawei.mytestapp.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.components.ComponentContainer.LayoutConfig;
import ohos.bundle.AbilityInfo;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.Camera;
import ohos.media.camera.device.CameraConfig;
import ohos.media.camera.device.CameraStateCallback;
import ohos.media.camera.device.FrameConfig;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.media.image.Image;
import ohos.media.image.ImageReceiver;
import ohos.media.image.common.ImageFormat;

import java.nio.ByteBuffer;

import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PREVIEW;

public class MainAbilitySlice extends AbilitySlice {

    private static final int ID_TEXT = 0x234567;// Text控件ID
    private DirectionalLayout myLayout = new DirectionalLayout(this);
    private ImageReceiver imageReceiver;
    private CameraKit cameraKit;
    private Camera mCamera;
    private SurfaceProvider surfaceview1;// 两个SurfaceProvider分别用于显示编码前后的视频
    private SurfaceProvider surfaceview2;
    private Surface previewsurface;

    private static final int ID_BUTTON = 0x1234;// Button控件ID
    private static final int VIDEO_WIDTH = 640;
    private static final int VIDEO_HEIGHT = 480;
    VDEncoder vdEncoder = new VDEncoder(15);// 创建编码类对象

    private SurfaceOps.Callback callback = new SurfaceOps.Callback() {
        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            previewsurface = surfaceOps.getSurface();
            //-------------------------相机相关
            openCamera();
            try {      // 获取当前设备的逻辑相机列表
                String[] cameraIds = cameraKit.getCameraIds();
                if (cameraIds.length <= 0) {
                    System.out.println("cameraIds size is 0");
                }
                // 创建相机运行时的回调
                CameraStateCallbackImpl cameraStateCallback = new CameraStateCallbackImpl();
                if(cameraStateCallback ==null) {
                    System.out.println("cameraStateCallback is null");
                }
                EventHandler eventHandler = new EventHandler(EventRunner.create("CameraCb"));
                if(eventHandler ==null) {
                    System.out.println("eventHandler is null");
                }
                // 根据上述设置的运行时的回调，创建相机设备
                cameraKit.createCamera(cameraIds[0], cameraStateCallback, eventHandler);
            } catch (IllegalStateException e) {
                System.out.println("getCameraIds fail");
            }
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int i, int i1, int i2) {
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {
        }
    };

    private final ImageReceiver.IImageArrivalListener imagerArivalListener = new ImageReceiver.IImageArrivalListener() {
        @Override
        public void onImageArrival(ImageReceiver imageReceiver) {// 当相机开始运行后，用于监听，实时返回视频原始数据
            mLog.log("imagearival", "arrival");
            Image mImage = imageReceiver.readNextImage();// 用于读取视频画面
            if(mImage != null){
                ByteBuffer mBuffer;
                byte[] YUV_DATA = new byte[VIDEO_HEIGHT * VIDEO_WIDTH * 3 / 2];// 存放从相机获取的原始 YUV 视频数据
                int i;

                // 从相机获取实时拍摄的视频数据，并将 Image 读取到的视频流数据存放在 mBuffer 中
                mBuffer = mImage.getComponent(ImageFormat.ComponentType.YUV_Y).getBuffer();
                mLog.log("imagearival", "***********获取到的数据："+mBuffer);
                System.out.println("***********获取到的数据："+mBuffer);
                // 从视频流mBuffer逐个读取成 byte 数组的形式，并存储在 YUV_DATA 中
                for(i=0;i< VIDEO_WIDTH * VIDEO_HEIGHT;i++){
                    YUV_DATA[i] = mBuffer.get(i);
                }
                mBuffer = mImage.getComponent(ImageFormat.ComponentType.YUV_V).getBuffer();
                for(i=0;i< VIDEO_WIDTH * VIDEO_HEIGHT / 4;i++){
                    YUV_DATA[(VIDEO_WIDTH * VIDEO_HEIGHT) + i * 2] = mBuffer.get(i * 2);
                }
                mBuffer = mImage.getComponent(ImageFormat.ComponentType.YUV_U).getBuffer();
                for(i=0;i< VIDEO_WIDTH * VIDEO_HEIGHT / 4;i++){
                    YUV_DATA[(VIDEO_WIDTH * VIDEO_HEIGHT)  + i * 2 + 1] = mBuffer.get(i * 2);
                }
                // 将视频数据 YUV_DATA 加入到队列等待编解码
                vdEncoder.addFrame(YUV_DATA);

                mImage.release();// 获取完视频数据之后及时释放
                return;
            }
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);
        //-------------------------获取权限如相机和录音
        String[] permission = {"harmonyos.permission.RECORD_AUDIO","harmonyos.permission.CAMERA"};
        for (int i=0;i<permission.length;i++){
            if(verifyCallingOrSelfPermission(permission[i]) != 0){
                if(canRequestPermission(permission[i])){
                    requestPermissionsFromUser(permission, 0);
                }
            }
        }
        //-------------------------页面总体布局
        myLayout.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);// 设置布局大小
        myLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_PARENT);
        LayoutConfig config = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        myLayout.setLayoutConfig(config);// 设置布局属性
        myLayout.setOrientation(Component.VERTICAL);
        myLayout.setPadding(32,32,32,32);// 设置布局内边距
        //------------------------添加Button按钮，用来控制是否开始进行编解码
        Button button = new Button(this);// 声明 Button 控件
        button.setLayoutConfig(config);// 设置 Button 属性
        button.setText("开始编解码.");
        button.setTextSize(50);
        button.setMarginLeft(350);
        button.setId(ID_BUTTON);// 设置 Button ID
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(0xFF51A8DD));// 设置控件背景颜色
        background.setCornerRadius(25);// 设置控件圆角
        button.setBackground(background);// 将背景set到Button中
        button.setPadding(10, 10, 10, 10);// 设置布局内边距
        myLayout.addComponent(button);// 将 Button 添加到布局中
        //-------------------------添加text文本框，用来显示编解码状态
        Text text = new Text(this);// 声明Text控件
        text.setLayoutConfig(config);
        text.setMarginTop(30);
        text.setMarginLeft(200);
        text.setText("这里显示编解码状态...");
        text.setTextSize(50);// 设置 Text 中字体大小
        text.setId(ID_TEXT);
        myLayout.addComponent(text);// 将 Text 添加到布局中

        //-------------------------添加两个surface UI用来显示视频
        surfaceview1 = new SurfaceProvider(this);
        surfaceview1.setWidth(400);// 设置 SurfaceProvider 大小
        surfaceview1.setHeight(300);
        surfaceview1.getSurfaceOps().get().addCallback(callback);// 设置回调
        surfaceview1.pinToZTop(true);
        surfaceview1.setRotation(180);// 设置画面旋转角度
        surfaceview1.setMarginTop(30);
        surfaceview1.setMarginLeft(300);
        myLayout.addComponent(surfaceview1);// 添加到布局中

        surfaceview2 = new SurfaceProvider(this);
        surfaceview2.setWidth(400);
        surfaceview2.setHeight(300);
        surfaceview2.pinToZTop(true);
        surfaceview2.setRotation(180);
        surfaceview2.setMarginTop(50);
        surfaceview2.setMarginLeft(300);
        myLayout.addComponent(surfaceview2);


        //        surfaceview2.getSurfaceOps().get().addCallback();
        takePictureInit();

        // 准备初始化解码器，并将解码后的视频显示在 surfaceview2 中
        vdEncoder.prepareDecoder(surfaceview2);

        button.setClickedListener(component -> {
            // 按钮被点击
            mLog.log("button", "start");
            vdEncoder.start();// 开始编码
            if(vdEncoder.isRuning){// 如果编码正在进行，显示当前编码状态
                text.setText("成功进行编解码，并显示在下方");
            }

        });

        super.setUIContent(myLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void takePictureInit() {
        // 创建 ImageReceiver 对象，注意 creat 函数中宽度要大于高度；5 为最大支持的图像数，请根据实际设置。
        imageReceiver = ImageReceiver.create(VIDEO_WIDTH,  VIDEO_HEIGHT, ImageFormat.YUV420_888, 10);
        imageReceiver.setImageArrivalListener(imagerArivalListener);
    }

    /**
     * 打开相机，获取 CameraKit 对象
     */
    private void openCamera(){
        cameraKit = CameraKit.getInstance(this);
        if (cameraKit == null) {
            return;
        }
    }

    /**
     * 相机回调
     */
    private final class CameraStateCallbackImpl extends CameraStateCallback {
        // 创建相机设备
        @Override
        public void onCreated(Camera camera) {
            CameraConfig.Builder cameraConfigBuilder = camera.getCameraConfigBuilder();
            if (cameraConfigBuilder == null) {
                System.out.println("onCreated cameraConfigBuilder is null");
                return;
            }
            // 配置预览的 Surface
            cameraConfigBuilder.addSurface(previewsurface);
            // 配置拍照的 Surface
            cameraConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            try {              // 相机设备配置
                camera.configure(cameraConfigBuilder.build());
            } catch (IllegalArgumentException e) {
                System.out.println("Argument Exception");
            } catch (IllegalStateException e) {
                System.out.println("State Exception");
            }
        }
        //相机配置
        @Override
        public void onConfigured(Camera camera) {
            mCamera = camera;
            FrameConfig.Builder frameConfigBuilder = mCamera.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
            // 配置预览 Surface
            frameConfigBuilder.addSurface(previewsurface);
            frameConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            FrameConfig previewFrameConfig = frameConfigBuilder.build();

            try {              // 启动循环帧捕获
                int triggerId = mCamera.triggerLoopingCapture(previewFrameConfig);
            } catch (IllegalArgumentException e) {
                System.out.println("Argument Exception");
            } catch (IllegalStateException e) {
                System.out.println("State Exception");
            }
        }
        // 释放相机设备
        @Override
        public void onReleased(Camera camera) {
            if (camera != null) {
                camera.release();
            }
        }
    }

}
